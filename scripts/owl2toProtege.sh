#!/bin/bash
set -e
declare -r ONTOLOGY=mmo.owl
declare -r ONTOLOGY_P35=mmo_p35.owl

if [[ -z "${SAXON_HOME}" ]]; then
echo "Last ned Saxon og sett env SAXON_HOME. Bruk filnavn Saxon9HE.jar. Se https://mvnrepository.com/artifact/net.sf.saxon/Saxon-HE"
#exit 1;
fi

if [[ ${ONTOLOGY} -nt ${ONTOLOGY_P35} || ! -f ${ONTOLOGY_P35} ]]; then
pwd
java -jar $SAXON_HOME/Saxon9HE.jar -s:${ONTOLOGY} -xsl:scripts/owl2toOwl.xsl -o:${ONTOLOGY_P35}
echo "${ONTOLOGY_P35}" oppdatert fra ${ONTOLOGY}
fi
