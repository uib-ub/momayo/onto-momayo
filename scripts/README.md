# Script for å håndtere OWL 2 -> OWL 1 og Protege 3.5

Kjør `init.sh` fra onto-momayo mappen for å legge inn hooks på post-commit og merge. En vil da oppdatere ontology fra owl 2 når en puller eller selv commiter.
