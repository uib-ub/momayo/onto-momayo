<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:mode on-no-match="shallow-copy"/>
    <xsl:key name="inverse" match="rdf:RDF/owl:*" use="owl:inverseOf/@rdf:resource"/>
    <!-- insert inverse properties-->
    <xsl:template match="*/owl:*[matches(name(),'property','i') and key('inverse',@rdf:about) and not(owl:inverseOf)]">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
            <owl:inverseOf>
               <owl:ObjectProperty rdf:about="{key('inverse',@rdf:about)/@rdf:about}"/>
            </owl:inverseOf>
<xsl:text>    
</xsl:text>
        </xsl:copy>
    </xsl:template>    
    
    <!-- delete named individuals -->
    <xsl:template match="owl:NamedIndividual"/>
</xsl:stylesheet>